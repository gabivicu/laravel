@extends('layouts.app')

@section('content')
        <h1>Create Question</h1>

        <form action="{{route('question.save')}}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" value="{{$quiz->id}}" name="quiz_id">
                    <div class="col-12-xl">
                    <input type="text" name="content" size="135">
                    </div>
                <br />
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
        
@endsection