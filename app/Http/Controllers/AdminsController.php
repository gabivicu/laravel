<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;

class AdminsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show ']]);
    }

}
