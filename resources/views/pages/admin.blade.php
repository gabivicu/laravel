@extends('layouts.app')

@section('content')
       <h1>{{$title}}</h1>
       <div class="container-fluid">
            <div class="row">
              <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                  <ul class="nav flex-column">
                    <li class="nav-item">
                      <a class="btn btn-primary" href="/admin/quiz" href="#">
                        Quiz
                      </a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
        </div>
@endsection
