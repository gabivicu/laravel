@extends('layouts.app')

@section('content')
    <br />
    <h4>Good job {{$test->name}} !!!</h4>
    <br />
    <h4>{{$quiz->title}} finished in {{$test->test_time}} seconds</h4>
    <br />
    <h4 ><span style='color:green;'>Wright answers : {{$corect_answers}}</span> \ <span style='color:red;'>Wrong answers : {{$incorect_answers}}</span></h4>
    <br /><br />
    @foreach($processed_answers as $processed_answer)
        @if($processed_answer->answer_is_correct)
            <div style='color:green;'>
                {{$processed_answer->question->content}}
            </div>
        @else
            <div style='color:red;'>
                {{$processed_answer->question->content}}
            </div>
        @endif
        <hr />
    @endforeach
@endsection


