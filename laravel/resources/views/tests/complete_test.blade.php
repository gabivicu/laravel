@extends('layouts.app')

@section('content')
        <br />
        <a href="/tests" class="btn btn-primary">Go Back</a>
        <br />
        <div class="card-body">
                <h1>{{$quiz->title}}</h1>
                <p>{{$quiz->created_at}}</p>
                <br />
                @if($questions->count() > 0)
        <form action="{{route('tests.save')}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" value="{{$quiz->id}}" name="quiz_id">
                <h4>Enter you name and start test:</h4><input type="text" name="name" value=""><br> <br />
                        @foreach($questions as $question)
                                <div class="well">
                                <h4>{{$question->id}} {{$question->content}}</h4>

                                        @if($question->answer->count() > 0)
                                                <table class="table table-striped">
                                                        <tr>
                                                                <td>Number</td>
                                                                <th>Content</th>
                                                                <th>Answer</th>
                                                        </tr>

                                                        @foreach($question->answer as $key => $answer)
                                                                <tr>
                                                                        <td>{{$key + 1}}</td>
                                                                        <td><h5>{{$answer->content}}</h5></td>
                                                                        <td><input type="radio" name="answer[{{$answer->question_id}}]" value="{{$answer->id}}"></td>
                                                                </tr>
                                                        @endforeach

                                                </table>
                                        @endif

                                </div>
                        @endforeach
                        {{-- {{ $questions->links() }} --}}
                        {{-- {{ $questions->appends(request()->except('page'))->links() }}. --}}

                        @else
                                <p>There are no questions !</p>
                        @endif
        <br />
        {{-- <a href="{{route('question.create', ['id' => $quiz->id])}}" class="btn btn-primary">Submit</a> --}}
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>

@endsection
