<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;


class PagesController extends Controller
{
    public function index(){
        $title = 'Welcome to Laravel ! ';
       // return view('pages.index', compact('title'));
        return view('pages.index')->with('title', $title);
    }

    public function tests(){
        // $title = 'This is about page';`


        $quizzes = Quiz::all();
        $title = 'Test page';
        return view('pages.tests')->with(
            [
                'title'   => $title,
                'quizzes' => $quizzes
            ]
            );
    
        // return view('pages.tests')->with('title', $title);
    }

    public function services(){
       // $title = 'This is services page';
       $data = array(
           'title' => 'Services',
           'services' => ['Web Design', 'Programming', 'SEO', 'Big Data', 'A.I.']
       );
        return view('pages.services')->with($data);
    }

    public function admin(){
        $title = 'Admin backoffice';
        return view('pages.admin')->with('title', $title);
    }

}


