@extends('layouts.app')

@section('content')
        <br />
        <a href="/admin" class="btn btn-primary">Go Back</a>
        <br />
        <h1>{{$title}}</h1>
        @if(count($quizzes) > 0)
                @foreach($quizzes as $quiz)
                        <div class="well">
                        <h3><a href="/admin/show_quiz/{{$quiz->id}}">{{$quiz->title}}</a></h3> <br />
                        </div>
                @endforeach
        @else
                <p>There are no quizzes to be shown !</p>
        @endif
@endsection
