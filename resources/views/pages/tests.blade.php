@extends('layouts.app')

@section('content')
        <br />
        <h1>{{$title}}</h1>
        @if(count($quizzes) > 0)
                @foreach($quizzes as $quiz)
                        <div class="well">
                                <input type="hidden" value="{{$quiz->id}}" name="id">
                            <h3><a href="/tests/{{$quiz->id}}/complete_test">{{$quiz->title}}</a></h3> <br />
                        </div>
                @endforeach
        @else
                <p>There are no quizzes to be shown !</p>
        @endif
@endsection
