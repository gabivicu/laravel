<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/tests', 'PagesController@tests');
Route::get('/services', 'PagesController@services');
Route::get('/admin', 'PagesController@admin');

Route::resource('posts', 'PostsController');


Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

// Routes for Admin page
Route::get('/admin/quiz', [
    'as' => 'quiz.index', 'uses' => 'QuizzesController@index'
]);

Route::get('/admin/show_quiz/{id}',[
    'as' => 'admin.show_quiz', 'uses' => 'QuizzesController@show_quiz'
]);

Route::get('/admin/quiz/{id}/create_question', [
    'as' => 'question.create', 'uses' => 'QuizzesController@create_question'
    ]);

Route::post('/admin/quiz/save_question', [
    'as' => 'question.save', 'uses' => 'QuizzesController@save_question'
    ]);

Route::get('/admin/quiz/{id}/create_answer/{question_id}', [
    'as' => 'answer.create', 'uses' => 'QuizzesController@create_answer'
]);

Route::post('/admin/quiz/save_answer',[
    'as' => 'answer.save', 'uses' => 'QuizzesController@save_answer'
]);

Route::get('/admin/quiz/{id}/edit_answer/{question_id}', [
    'as' => 'answer.edit', 'uses' => 'QuizzesController@edit_answer'
]);

Route::post('/admin/quiz/update_answer',[
    'as' => 'answer.update', 'uses' => 'QuizzesController@update_answer'
]);

Route::get('/admin/quiz/delete_answer',[
    'as' => 'answer.delete', 'uses' => 'QuizzesController@delete_answer'
]);

// Routes for Tests page

Route::get('/tests/{quiz_id}/complete_test', [
    'as' => 'tests.complete', 'uses' => 'TestsController@complete_test'
]);

Route::post('/tests/save_test', [
    'as' => 'tests.save', 'uses'  => 'TestsController@save_test'
]);

Route::get('/tests/{quiz_id}/result_test', [
    'as' => 'tests.result', 'uses' => 'TestsController@result_test'
]);

Route::get('stergeSesiune', function(){
    return Session::flush();
});
