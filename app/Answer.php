<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
        // Table Name
        protected $table = 'answers';
        // Primary key
        public $primaryKey = 'id';
        // Timestamps
        public $timestamps = true;

        public function question()
        {
            return $this->belongsTo('App\Question');
        }
}
