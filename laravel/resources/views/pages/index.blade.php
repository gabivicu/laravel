@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center">
        <h1>{{$title}}</h1>
        <p>Aplicatie Laravel 5.4</p>
        <p>
                <a href="#" role="button" href="/login" class="btn btn-primary">Login</a>
                <a href="#" role="button" href="/register" class="btn btn-success">Register</a>
              </p>
    </div>
@endsection
