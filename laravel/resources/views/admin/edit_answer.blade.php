@extends('layouts.app')

@section('content')
        <br />
        <a href="{{route('admin.show_quiz', $question->quiz_id)}}" class="btn btn-primary">Go Back</a>
        <h1>Edit Answers</h1>
        <h1>{{$question->content}}</h1>
        <form action="{{route('answer.update')}}" method="POST">
                {{ csrf_field() }}
                        <input type="hidden" value="{{$question->id}}" name="question_id">
                         <input type="hidden" value="{{$answer->id}}" name="answer_id">
                          <input type="hidden" value="{{$question->quiz_id}}" name="quiz_id">
                                <div class="form-group">
                                        <textarea class="form-control" name="content" rows="4" cols="10">{{$answer->content}}</textarea> 
                                </div>
                                <div class="form-group">
                                        <label class="radio-inline"><input type="radio" name="status" {{ ($answer->status == 1) ? 'checked' : ''}} value="1" >Right</label>
                                        <label class="radio-inline"><input type="radio" name="status" {{ ($answer->status == 0) ? "checked" : "" }} value="0" >Wrong</label>  
                                </div>
                <button type="submit" class="btn btn-primary">Save</button>
        </form>
@endsection
