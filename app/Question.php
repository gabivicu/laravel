<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
        // Table Name
        protected $table = 'questions';
        // Primary key
        public $primaryKey = 'id';
        // Timestamps
        public $timestamps = true;

        public function answer()
        {
                return $this->hasMany('App\Answer', 'question_id', 'id');
        }
}
