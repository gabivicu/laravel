@extends('layouts.app')

@section('content')
        <br />
        <a href="{{route('admin.show_quiz', $questions->quiz_id)}}" class="btn btn-primary">Go Back</a>
        <h1>Add Answers</h1><br />
        <h1>{{$questions->content}}</h1>
                <form action="{{route('answer.save')}}" method="POST">
                {{ csrf_field() }}
                    <input type="hidden" value="{{$questions->id}}" name="question_id">
                    <input type="hidden" value="{{$questions->quiz_id}}" name="quiz_id">
                                <div class="form-group" >
                                        <textarea class="form-control" name="content" rows="4" cols="10" placeholder="Add answer here !"></textarea> 
                                </div>
                                <div class="form-group" >
                                        <label class="radio-inline"><input type="radio" name="status" value="1" checked>Right</label>
                                        <label class="radio-inline"><input type="radio" name="status" value="1">Wrong</label>  
                                </div>
                    <br />
            <button type="submit" class="btn btn-primary">Save</button>
                </form>
        
@endsection