<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Question;
use App\Answer;
use App\Http\Controllers\Controller;

class QuizzesController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index() {

        $quizzes = Quiz::all();
        $title   = 'Quizzes Page';
        return view('admin.quiz')->with(
            [
                "title"   => $title,
                'quizzes' => $quizzes
            ]
        );
    }


    public function show_quiz($id) {

        $questions = Question::with(['answer'])->where('quiz_id', $id)->get();
        $quiz      = Quiz::find($id);
        $questions = Question::orderBy('created_at', 'asc')->paginate(5);


        return view('admin.show_quiz')->with(
            [
                'quiz'      => $quiz,
                'questions' => $questions
            ]
            );

    }

    public function create_question($id) {
        $quiz = Quiz::find($id);
        return view('admin.create_question')->with(
            [
                'quiz' => $quiz
            ]
            );
    }


    public function save_question(Request $request){

        $request->validate([
            'quiz_id' => 'required|numeric',
            'content' => 'required|min:5',
        ]);

        $questions          = new Question;
        $questions->content = $request->input('content');
        $questions->quiz_id = $request->input('quiz_id');
        $questions->save();

        return redirect('/admin/show_quiz/' . $questions->quiz_id)->with('success', 'Question Created !');
    }


        public function create_answer($id, $question_id) {
            $quizzes   = Quiz::find($id);
            $questions = Question::find($question_id);
            return view('admin.create_answer')->with(
                [
                    'questions' => $questions,
                    'quizzes'   => $quizzes,
                ]
                );

        }

        public function save_answer(Request $request){
                $request->validate([
                    'quiz_id'     => 'required|numeric',
                    'question_id' => 'required|numeric',
                    'content'     => 'required|min:5',
                    'status'      => 'required',
                ]);

                $questions            = new Question;
                $questions->quiz_id   = $request->input('quiz_id');
                $answers              = new Answer;
                $answers->content     = $request->input('content');
                $answers->question_id = $request->input('question_id');
                $answers->status      = $request->input('status');
                $answers->save();

                return redirect('/admin/show_quiz/' . $questions->quiz_id)->with('success', 'Answer Created !');
        }

        public function edit_answer($id, $question_id){
            $answer    = Answer::find($id);
            $quiz      = Quiz::find($id);
            $question  = Question::find($question_id);
            return view('admin.edit_answer')->with(
                [
                    'question' => $question,
                    'quiz'     => $quiz,
                    'answer'   => $answer,
                ]
                );
        }

        public function update_answer(Request $request){
            $request->validate([
                'answer_id'     => 'required',
                'content'       => 'required|min:5',
                'status'        => 'required',
            ]);


            if($request->input('status') == 1){
                        Answer::where('status',1)
                       ->where('question_id', $request->input('question_id'))
                       ->update(['status' => 0]);
            }

            $answer          = Answer::find($request->input('answer_id'));
            $answer->content = $request->input('content');
            $answer->status  = $request->input('status');


            $answer->save();

            return redirect('admin/show_quiz/' . $request->input('quiz_id'))->with('success', 'Answer Updated !');
        }

        public function delete_answer(Request $request){
            $answer      = Answer::find($request->input('id'));
            $answer->delete();
            return redirect('admin/show_quiz/' . $request->input('quiz_id'))->with('success', 'Answer Deleted !');
        }

}
