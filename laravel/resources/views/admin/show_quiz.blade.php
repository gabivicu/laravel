@extends('layouts.app')

@section('content')
        <br />
        <a href="{{route('quiz.index')}}" class="btn btn-primary">Go Back</a>
        <br />
                <div class="card-body">
                        <h1>{{$quiz->title}}</h1>
                        <p>{{$quiz->created_at}}</p>
                        <br />
                        @if($questions->count() > 0)
                        @foreach($questions as $question)
                                <div class="well">
                                <h4>{{$question->id}} {{$question->content}}</h4>

                                        @if($question->answer->count() > 0)
                                                <table class="table table-striped">
                                                        <tr>
                                                                <td>Number</td>
                                                                <th>Content</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                        </tr>

                                                        @foreach($question->answer as $key => $answer)
                                                                <tr>
                                                                        <td>{{$key + 1}}</td>
                                                                        <td><h5>{{$answer->content}}</td>
                                                                        <td><p><span class="{{ ($answer->status == 1) ? 'badge badge-primary' : 'badge badge-danger'}}" >{{ ($answer->status == 1) ? 'Right' : 'Wrong'}}</span></p></td>
                                                                        <td><a href="{{route('answer.edit', ['id' => $answer->id, 'question_id' => $question->id, 'quiz_id' => $question->quiz_id])}}" class="">Edit</a>
                                                                                 |
                                                                        <a href="{{route('answer.delete', ['id' => $answer->id, 'question_id' => $question->id, 'quiz_id' => $question->quiz_id])}}" onclick="return confirm('Are you sure you want to delete ?')">Delete</a>
                                                                        </td>
                                                                </tr>
                                                        @endforeach

                                                </table>
                                        @endif

                                <a href="{{route('answer.create', ['id' => $quiz->id, 'question_id' => $question->id])}}" class="btn btn-primary">Add Answers</a><br /><br />
                                </div>
                        @endforeach
                        {{ $questions->links() }}
                        @else
                                <p>There are no questions !</p>
                        @endif
                </div>
        <br /><a href="{{route('question.create', ['id' => $quiz->id])}}" class="btn btn-primary">Create New Question</a>

@endsection
