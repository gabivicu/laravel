<?php
/*
    - validarea formularului
    - validarea chestionarului (numarul de raspunsuri corecte, cat timp a durat )
    - salvez in baza de date
    - coloana durata si rezultata final(numar de raspunsuri corecte/gresite)

    Admin listare tabelara a testelor date
        - listare raspuns intrebari cu corecte/gresite
        -


*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\Question;
use App\Answer;
use App\Test;

class TestsController extends Controller
{

    public function index(){

        $quizzes = Quiz::all();
        return view('tests.test')->with(
            [
                'title'   => $title,
                'quizzes' => $quizzes
            ]
            );
    }

    public function complete_test(Request $request,$id){

        if(!$request->session()->get('start_time'. $id)){
            $request->session()->put('start_time' . $id, time());
        }
        $start_time = time();

        $questions          = Question::with(['answer'])->where('quiz_id', $id)->get();  //answer este functia din modelul Question
        $quiz               = Quiz::find($id);
        // $questions = Question::orderBy('created_at', 'asc')->paginate(4);

        return view('tests.complete_test')->with(
            [
                'quiz'      => $quiz,
                'questions' => $questions,
            ]
            );

    }


    public function save_test(Request $request){
       $test_time = time() - $request->session()->get('start_time' . $request->quiz_id);
       // Aici sterg sesiunea
       $request->validate([
                'name'      => 'required|min:5',
                'quiz_id'   => 'required'
        ]);
            $answer         = new Answer;
            $test           = new Test;

        $test->name         = $request->input('name');
        $test->quiz_id      = $request->input('quiz_id');
        $test->result       = json_encode($request->input('answer'));
        $test->test_time    = date('H:i:s', $test_time);
        $test->save();
        return redirect('/tests/' . $test->id . '/result_test')->with('success', 'Test completed ! ');
    }

    public function result_test($id){
        $test = Test::where('id', $id)->first();
    //    dd(json_decode($test->result));

        $corect_answers = 0;
        $incorect_answers = 0;

       foreach(json_decode($test->result) as $question_id => $answer_id){
            $res                 = new \stdClass();
            $res->question       = Question::find($question_id);
            $res->given_answer   = Answer::find($answer_id);
            $res->correct_answer = Answer::where(['question_id'=> $question_id, 'status'=>1])->first();

        //    dd($res->correct_answer);

            if($res->given_answer->id == $res->correct_answer->id){
                $res->answer_is_correct = true;
                $corect_answers++;
            } else {
                $res->answer_is_correct = false;
                $incorect_answers++;
            }
            $processed_answers[] = $res;
       }
    //   dd($processed_answers);
        $quiz = Quiz::where('id', $test->quiz_id)->first();
        return view('tests.result_test')->with([
                'test' => $test,
                'quiz' => $quiz,
                'processed_answers' => $processed_answers,
                'corect_answers'    => $corect_answers,
                'incorect_answers'  => $incorect_answers
        ]
        );
    }
}
